<?php namespace Smartsoftware\UserActivityLog;

use Illuminate\Support\ServiceProvider;

use Smartsoftware\UserActivityLog\Commands\MigrationCommand;

class UserActivityLogServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->commands('command.useractivitylog.migration');

        $this->publishes([
            __DIR__.'/config/config.php' => config_path('useractivitylog/config.php'),
        ]);
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
        $this->app->singleton('UserActivityLog', function($app){
            return new UserActivityLog();
        });

        $this->registerCommands();
	}

	/**
     * Register the artisan commands.
     *
     * @return void
     */
    private function registerCommands()
    {
        $this->app->singleton('command.useractivitylog.migration', function ($app) {
            return new MigrationCommand();
        });
    }


	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('command.useractivitylog.migration');
	}
}