<?php namespace Smartsoftware\UserActivityLog;

interface ActivityLogInterface {
    public function getActivityLogForEvent($event, $model);
}