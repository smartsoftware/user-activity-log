<?php namespace Smartsoftware\UserActivityLog;

use Request;
use Smartsoftware\UserActivityLog\Model\UserActivityLog as ActivityLog;
use Smartsoftware\UserActivityLog\Model\UserActivityLogModel as ActivityLogModel;
use Carbon\Carbon;
use Config;

class UserActivityLog {

    protected $extra_filters = array();

    public function setExtraFilter($name, $value) {
        $this->extra_filters[$name] = $value;
    }

    public function log($text, $obj = null, $user = null, $extra = array())
    {
        if (!$user) {
            $get_current_user = Config::get('useractivitylog.config.get_current_user');
            $user = $get_current_user();

            if(!$user) {
                throw new \Exception('Cant get user for logging');
            }
        }

        $log = new ActivityLog();

        if ($obj) {
            $activitylogModel = ActivityLogModel::firstOrCreate(['model' => get_class($obj)]);
            $log->model()->associate($activitylogModel);
            $log->model_id = $obj->id;
        }

        $log->ip_address = Request::getClientIp();
        $log->text       = $text;
        $log->moment     = new Carbon();
        $log->user_id    = $user->id;

        foreach ($this->extra_filters as $name => $value) {
            $log->$name = $value;
        }

        foreach ($extra as $name => $value) {
            $log->$name = $value;
        }

        return $log->save();
    }

    public function query($extras=[])
    {
        $q = ActivityLog::query();

        foreach ($this->extra_filters as $name => $value) {
            $q->where($name, $value);
        }

        foreach ($extras as $name => $value) {
            $q->where($name, $value);
        }

        return $q;
    }

    public function clean($older_than_days)
    {
        $date = new Carbon();
        $date->subDays($older_than_days);
        ActivityLog::where('moment','<',$date)->delete();
    }
}