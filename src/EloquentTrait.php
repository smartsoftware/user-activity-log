<?php namespace Smartsoftware\UserActivityLog;

trait EloquentTrait
{
    public $changedValues;

    public $LOG_ACTIVATED = true;

    public static $LOG_EXTRAS_FIELDS = array();

    /**
     * Set Logging activation status
     *
     * @param boolean $f
     */
    public function logging_activated($f)
    {
        $this->LOG_ACTIVATED = $f;
    }

    /**
     * Add extra fields only for this model
     *
     * @param $name
     * @param $value
     */
    public static function addLogExtraFields($name, $value)
    {
        self::$LOG_EXTRAS_FIELDS[$name] = $value;
    }

    protected static function bootEloquentTrait()
    {
        foreach (static::getUserActivityLogEvents() as $eventName) {
            static::$eventName(function (ActivityLogInterface $model) use ($eventName) {
                //if it's not activated we go back
                if (!$model->LOG_ACTIVATED) return;
                // get message
                $message = $model->getActivityLogForEvent($eventName, $model);

                if ($message != '') {
                    $log = app()->make('UserActivityLog');
                    $log->log($message, $model, null, self::$LOG_EXTRAS_FIELDS);
                }
            });

            if ($eventName == 'updated') {
                static::updating(function($model) {
                    //if it's not activated we go back
                    if (!$model->LOG_ACTIVATED) return;

                    $changed = [];

                    foreach ($model->getDirty() as $attribute => $value) {
                        $original = $model->getOriginal($attribute);
                        $changed[$attribute] = ['from' => $original, 'to' => $value];
                    }

                    $model->changedValues = $changed;
                });
            }
        }
    }

    public function renderUpdated($template, $ignore = [], $relations = [])
    {
        $cambios = '';

        foreach($this->changedValues as $attr => $chg) {
            if (in_array($attr, $ignore)) continue;
            $chg['attr'] = $attr;

            if(array_key_exists($attr, $relations)) {
                list($rel, $field) = explode('.',$relations[$attr]);
                $related = $this->$rel;
                if ($related) {
                    $chg['to'] = $related->$field;
                }
                if ($chg['from']) {
                    $chg['from'] = $related->find($chg['from'])->$field;
                }
            }

            $cambios .= str_replace(['{from}', '{to}', '{attr}'], $chg, $template);
        }

        return $cambios;
    }

    protected static function getUserActivityLogEvents()
    {
        if (isset(static::$recordEvents)) {
            return static::$recordEvents;
        }
        return [
            'created', 'updated', 'deleting', 'deleted',
        ];
    }
}