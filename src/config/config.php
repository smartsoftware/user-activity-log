<?php
return array(
    /**
     * User Model
     */
    'user_model' => 'User',
    'get_current_user' => function() {
        return Auth::user();
    }
);