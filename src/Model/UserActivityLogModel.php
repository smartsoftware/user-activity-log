<?php namespace Smartsoftware\UserActivityLog\Model;

use \Illuminate\Database\Eloquent\Model as Eloquent;

class UserActivityLogModel extends Eloquent{

    protected $fillable = ['model'];

    public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'activity_log_model';


    public function userActivityLogs()
    {
        return $this->hasMany('Smartsoftware\UserActivityLog\Model\UserActivityLog');
    }
}