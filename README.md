# User Activity Log for Laravel

![logo3.png](https://bitbucket.org/repo/ybo85g/images/3896452826-logo3.png)

### Installation & configuration ###

With composer:
```bash
    $ composer require smartsoftware/user-activity-log
```
**Publish config file:**
```bash
    $ php artisan view:publish smartsoftware/user-activity-log
```
**Run migrations**
```bash
    $ php artisan migrate --package='smartsoftware/user-activity-log'
```

**Add provider to config/app.php providers array**

    'Smartsoftware\UserActivityLog\UserActivityLogServiceProvider'

## Log Activity ##
```php
<?php

// Log activity for current user
UserActivityLog::log('Ticket modify');

// Log activity with attached model instance
$ticket = Ticket::find(15);
UserActivityLog::log('Ticket was edited', $ticket);

// Log activity to custom user
$user = User::find(1);
UserActivityLog::log('Ticket was edited', $ticket, $user);

```

## Retrieving Log ##
```php
<?php
use UserActivityLog;

// get all logs (lasts first)
$log = UserActivityLog::query()->latest()->get();

// get last 20 logs (lasts first)
$log = UserActivityLog::query()->latest()->take(20)->get();

// get all logs for current user
$log = UserActivityLog::query()->latest()->currentUser()->get();

// get logs related to a model instance
$ticket = Ticket::find(15);
$log = UserActivityLog::query()->latest()->related($ticket)->get();

// get logs related to a model class
$log = UserActivityLog::query()->latest()->related('Ticket')->get();

// and only from current user
$log = UserActivityLog::query()->latest()->related($ticket)->currentUser()->get();

// for other user
$user = User::find(1);
$log = UserActivityLog::query()->latest()->fromUser($user)->get();
```
**Geting log attached object**
```php
<?php
$log = UserActivityLog::query()->latest()->take(20)->get();
$related_obj = $log[0]->getObject();

```

## Cleaning Logs ##
```php
<?php
// clean logs older than 60 days
UserActivityLog::clean(60);
```